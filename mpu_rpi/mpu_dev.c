#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "mpu6050.h"

struct mpu_data {
	struct i2c_client *drv_client;
	int accel_values[3];
	int gyro_values[3];
	int temperature;
};

static struct mpu_data mpu6050_data;

static int mpu6050_read_data(void)
{
	int temp;
	struct i2c_client *drv_client = mpu6050_data.drv_client;

	if (drv_client == 0) {
		return -ENODEV;
	}

	/* accel */
	mpu6050_data.accel_values[0] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_ACCEL_XOUT_H));
	mpu6050_data.accel_values[1] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_ACCEL_YOUT_H));
	mpu6050_data.accel_values[2] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_ACCEL_ZOUT_H));
	/* gyro */
	mpu6050_data.gyro_values[0] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_GYRO_XOUT_H));
	mpu6050_data.gyro_values[1] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_GYRO_YOUT_H));
	mpu6050_data.gyro_values[2] = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_GYRO_ZOUT_H));
	/* Temperature */
	temp = (s16)((u16)i2c_smbus_read_word_swapped(drv_client, REG_TEMP_OUT_H));
	mpu6050_data.temperature = (temp + 12420 + 170) / 340;

	dev_info(&drv_client->dev, "sensor data read:\n");
	dev_info(&drv_client->dev, "ACCEL[X,Y,Z] = [%d, %d, %d]\n",
		mpu6050_data.accel_values[0],
		mpu6050_data.accel_values[1],
		mpu6050_data.accel_values[2]);
	dev_info(&drv_client->dev, "GYRO[X,Y,Z] = [%d, %d, %d]\n",
		mpu6050_data.gyro_values[0],
		mpu6050_data.gyro_values[1],
		mpu6050_data.gyro_values[2]);
	dev_info(&drv_client->dev, "TEMP = %d\n",
		mpu6050_data.temperature);

	return 0;
}

static int mpu6050_probe(struct i2c_client *drv_client,
			 const struct i2c_device_id *id)
{
	int ret;

	dev_info(&drv_client->dev,
		"i2c client address is 0x%X\n", drv_client->addr);

	/* Read who_am_i register */
	ret = i2c_smbus_read_byte_data(drv_client, REG_WHO_AM_I);
	if (IS_ERR_VALUE(ret)) {
		dev_err(&drv_client->dev,
			"i2c_smbus_read_byte_data() failed with error: %d\n",
			ret);
		return ret;
	}
	if (ret != MPU6050_WHO_AM_I) {
		dev_err(&drv_client->dev,
			"wrong i2c device found: expected 0x%X, found 0x%X\n",
			MPU6050_WHO_AM_I, ret);
		return -1;
	}
	dev_info(&drv_client->dev,
		"i2c mpu6050 device found, WHO_AM_I register value = 0x%X\n",
		ret);

	/* Setup the device */
	/* No error handling here! */
	i2c_smbus_write_byte_data(drv_client, REG_CONFIG, 0);
	i2c_smbus_write_byte_data(drv_client, REG_GYRO_CONFIG, 0);
	i2c_smbus_write_byte_data(drv_client, REG_ACCEL_CONFIG, 0);
	i2c_smbus_write_byte_data(drv_client, REG_FIFO_EN, 0);
	i2c_smbus_write_byte_data(drv_client, REG_INT_PIN_CFG, 0);
	i2c_smbus_write_byte_data(drv_client, REG_INT_ENABLE, 0);
	i2c_smbus_write_byte_data(drv_client, REG_USER_CTRL, 0);
	i2c_smbus_write_byte_data(drv_client, REG_PWR_MGMT_1, 0);
	i2c_smbus_write_byte_data(drv_client, REG_PWR_MGMT_2, 0);

	mpu6050_data.drv_client = drv_client;

	dev_info(&drv_client->dev, "i2c driver probed\n");

	return 0;
}

static int mpu6050_remove(struct i2c_client *drv_client)
{
	mpu6050_data.drv_client = 0;

	dev_info(&drv_client->dev, "i2c driver removed\n");
	return 0;
}

static const struct i2c_device_id mpu6050_idtable[] = {
	{ "mpu_dev", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, mpu6050_idtable);

static struct i2c_driver mpu6050_i2c_driver = {
	.driver = {
		.name = "mpu_dev",
	},

	.probe = mpu6050_probe,
	.remove = mpu6050_remove,
	.id_table = mpu6050_idtable,
};

static ssize_t accel_show(struct class *class,
			    struct class_attribute *attr, char *buf)
{
	mpu6050_read_data();

	sprintf(buf, "%6d %6d %6d\n", mpu6050_data.accel_values[0],
					  mpu6050_data.accel_values[1],
					  mpu6050_data.accel_values[2]);
	return strlen(buf);
}

static ssize_t gyro_show(struct class *class,
			   struct class_attribute *attr, char *buf)
{
	mpu6050_read_data();

	sprintf(buf, "%6d %6d %6d\n", mpu6050_data.gyro_values[0],
					  mpu6050_data.gyro_values[1],
					  mpu6050_data.gyro_values[2]);
	return strlen(buf);
}

static ssize_t temperature_show(struct class *class,
			 struct class_attribute *attr, char *buf)
{
	mpu6050_read_data();

	sprintf(buf, "%d\n", mpu6050_data.temperature);
	return strlen(buf);
}

CLASS_ATTR_RO(accel);
CLASS_ATTR_RO(gyro);
CLASS_ATTR_RO(temperature);

static struct class *attr_class;

static int mpu6050_init(void)
{
	int ret;

	/* Create i2c driver */
	ret = i2c_add_driver(&mpu6050_i2c_driver);
	if (ret) {
		pr_err("mpu_dev: failed to add new i2c driver: %d\n", ret);
		return ret;
	}
	pr_info("mpu_dev: i2c driver created\n");

	/* Create class */
	attr_class = class_create(THIS_MODULE, "mpu_dev");
	if (IS_ERR(attr_class)) {
		ret = PTR_ERR(attr_class);
		pr_err("mpu_dev: failed to create sysfs class: %d\n", ret);
		return ret;
	}
	pr_info("mpu_dev: sysfs class created\n");

	/* Create accel */
	ret = class_create_file(attr_class, &class_attr_accel);
	if (ret) {
		pr_err("mpu_dev: failed to create sysfs class attribute accel: %d\n", ret);
		return ret;
	}
	
	/* Create gyro */
	ret = class_create_file(attr_class, &class_attr_gyro);
	if (ret) {
		pr_err("mpu_dev: failed to create sysfs class attribute gyro: %d\n", ret);
		return ret;
	}

	/* Create temperature */
	ret = class_create_file(attr_class, &class_attr_temperature);
	if (ret) {
		pr_err("mpu_dev: failed to create sysfs class attribute temperature: %d\n", ret);
		return ret;
	}

	pr_info("mpu_dev: sysfs class attributes created\n");

	pr_info("mpu_dev: module loaded\n");
	return 0;
}

static void mpu6050_exit(void)
{
	if (attr_class) {
		class_remove_file(attr_class, &class_attr_accel);
		class_remove_file(attr_class, &class_attr_gyro);
		class_remove_file(attr_class, &class_attr_temperature);
		pr_info("mpu_dev: sysfs class attributes removed\n");

		class_destroy(attr_class);
		pr_info("mpu_dev: sysfs class destroyed\n");
	}

	i2c_del_driver(&mpu6050_i2c_driver);
	pr_info("mpu_dev: i2c driver deleted\n");

	pr_info("mpu_dev: module exited\n");
}

module_init(mpu6050_init);
module_exit(mpu6050_exit);

MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("mpu6050 Demo");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");


