#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

#define MATRIX_PIN_X1 6
#define MATRIX_PIN_X2 13
#define MATRIX_PIN_X3 19
#define MATRIX_PIN_X4 26
#define MATRIX_PIN_Y1 12
#define MATRIX_PIN_Y2 16
#define MATRIX_PIN_Y3 20
#define MATRIX_PIN_Y4 21

#define DEBOUNCE_TIME 10

static int irq_num1, irq_num2, irq_num3, irq_num4;

static char key_table[] = { '1', '2', '3', 'A',
				'4', '5', '6', 'B',
				'7', '8', '9', 'C',
				'*', '0', '#', 'D' };
static u64 last_matrix_time = 0;

static irqreturn_t matrix_isr(int irq, void *data)
{
	u8 y = 0;

	gpio_set_value(MATRIX_PIN_X1, 1);
	gpio_set_value(MATRIX_PIN_X2, 0);
	gpio_set_value(MATRIX_PIN_X3, 0);
	gpio_set_value(MATRIX_PIN_X4, 0);
	mdelay(1);
	y = gpio_get_value(MATRIX_PIN_Y1) * 4 +
		gpio_get_value(MATRIX_PIN_Y2) * 3 +
		gpio_get_value(MATRIX_PIN_Y3) * 2 +
		gpio_get_value(MATRIX_PIN_Y4);
	if (y) {
		y += 12;
		goto out;
	}

	gpio_set_value(MATRIX_PIN_X1, 0);
	gpio_set_value(MATRIX_PIN_X2, 1);
	gpio_set_value(MATRIX_PIN_X3, 0);
	gpio_set_value(MATRIX_PIN_X4, 0);
	mdelay(1);
	y = gpio_get_value(MATRIX_PIN_Y1) * 4 +
		gpio_get_value(MATRIX_PIN_Y2) * 3 +
		gpio_get_value(MATRIX_PIN_Y3) * 2 +
		gpio_get_value(MATRIX_PIN_Y4);
	if (y) {
		y += 8;
		goto out;
	}

	gpio_set_value(MATRIX_PIN_X1, 0);
	gpio_set_value(MATRIX_PIN_X2, 0);
	gpio_set_value(MATRIX_PIN_X3, 1);
	gpio_set_value(MATRIX_PIN_X4, 0);
	mdelay(1);
	y = gpio_get_value(MATRIX_PIN_Y1) * 4 +
		gpio_get_value(MATRIX_PIN_Y2) * 3 +
		gpio_get_value(MATRIX_PIN_Y3) * 2 +
		gpio_get_value(MATRIX_PIN_Y4);
	if (y) {
		y += 4;
		goto out;
	}

	gpio_set_value(MATRIX_PIN_X1, 0);
	gpio_set_value(MATRIX_PIN_X2, 0);
	gpio_set_value(MATRIX_PIN_X3, 0);
	gpio_set_value(MATRIX_PIN_X4, 1);
	mdelay(1);
	y = gpio_get_value(MATRIX_PIN_Y1) * 4 +
		gpio_get_value(MATRIX_PIN_Y2) * 3 +
		gpio_get_value(MATRIX_PIN_Y3) * 2 +
		gpio_get_value(MATRIX_PIN_Y4);
	if (y) {
		y += 0;
		goto out;
	}
out:
	if (y) {
		if (get_jiffies_64() > last_matrix_time + DEBOUNCE_TIME) {
			last_matrix_time = get_jiffies_64();
			printk(KERN_INFO "matrix: key=%c", key_table[--y]);
		}
	}
	
	gpio_set_value(MATRIX_PIN_X1, 1);
	gpio_set_value(MATRIX_PIN_X2, 1);
	gpio_set_value(MATRIX_PIN_X3, 1);
	gpio_set_value(MATRIX_PIN_X4, 1);

	return IRQ_HANDLED;
}

static int __init matrix_init(void)
{
	int ret;

	ret = gpio_request(MATRIX_PIN_X1, "MATRIX_PIN_X1");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X1, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X2, "MATRIX_PIN_X2");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X2, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X3, "MATRIX_PIN_X3");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X3, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X4, "MATRIX_PIN_X4");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X4, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y1, "MATRIX_PIN_Y1");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y1, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y2, "MATRIX_PIN_Y2");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y2, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y3, "MATRIX_PIN_Y3");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y3, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y4, "MATRIX_PIN_Y4");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y4, ret);
		return -1;
	}

	gpio_direction_output(MATRIX_PIN_X1, 0);
	gpio_direction_output(MATRIX_PIN_X2, 0);
	gpio_direction_output(MATRIX_PIN_X3, 0);
	gpio_direction_output(MATRIX_PIN_X4, 0);
	gpio_direction_input(MATRIX_PIN_Y1);
	gpio_direction_input(MATRIX_PIN_Y2);
	gpio_direction_input(MATRIX_PIN_Y3);
	gpio_direction_input(MATRIX_PIN_Y4);

	ret = gpio_to_irq(MATRIX_PIN_Y1);
	if (ret < 0) {
		printk(KERN_ERR "matrix: failed to request IRQ: %d\n", ret);
		return -1;
	}

	irq_num1 = ret;
	printk(KERN_INFO "matrix: requested IRQ#%d.\n", irq_num1);
	ret = request_irq(irq_num1, matrix_isr, IRQF_TRIGGER_RISING, "matrix_irq", NULL);
	if (ret) {
		printk(KERN_ERR "matrix: failed to request IRQ\n");
		return -1;
	}

	ret = gpio_to_irq(MATRIX_PIN_Y2);
	if (ret < 0) {
		printk(KERN_ERR "matrix: failed to request IRQ: %d\n", ret);
		return -1;
	}

	irq_num2 = ret;
	printk(KERN_INFO "matrix: requested IRQ#%d.\n", irq_num2);
	ret = request_irq(irq_num2, matrix_isr, IRQF_TRIGGER_RISING, "matrix_irq", NULL);
	if (ret) {
		printk(KERN_ERR "matrix: failed to request IRQ\n");
		return -1;
	}

	ret = gpio_to_irq(MATRIX_PIN_Y3);
	if (ret < 0) {
		printk(KERN_ERR "matrix: failed to request IRQ: %d\n", ret);
		return -1;
	}

	irq_num3 = ret;
	printk(KERN_INFO "matrix: requested IRQ#%d.\n", irq_num3);
	ret = request_irq(irq_num3, matrix_isr, IRQF_TRIGGER_RISING, "matrix_irq", NULL);
	if (ret) {
		printk(KERN_ERR "matrix: failed to request IRQ\n");
		return -1;
	}

	ret = gpio_to_irq(MATRIX_PIN_Y4);
	if (ret < 0) {
		printk(KERN_ERR "matrix: failed to request IRQ: %d\n", ret);
		return -1;
	}

	irq_num4 = ret;
	printk(KERN_INFO "matrix: requested IRQ#%d.\n", irq_num4);
	ret = request_irq(irq_num4, matrix_isr, IRQF_TRIGGER_RISING, "matrix_irq", NULL);
	if (ret) {
		printk(KERN_ERR "matrix: failed to request IRQ\n");
		return -1;
	}

	gpio_set_value(MATRIX_PIN_X1, 1);
	gpio_set_value(MATRIX_PIN_X2, 1);
	gpio_set_value(MATRIX_PIN_X3, 1);
	gpio_set_value(MATRIX_PIN_X4, 1);
	
	printk(KERN_INFO "matrix: module loaded\n");
	return 0;
}

static void __exit matrix_exit(void)
{
	free_irq(irq_num1, NULL);
	free_irq(irq_num2, NULL);
	free_irq(irq_num3, NULL);
	free_irq(irq_num4, NULL);

	gpio_free(MATRIX_PIN_X1);
	gpio_free(MATRIX_PIN_X2);
	gpio_free(MATRIX_PIN_X3);
	gpio_free(MATRIX_PIN_X4);
	gpio_free(MATRIX_PIN_Y1);
	gpio_free(MATRIX_PIN_Y2);
	gpio_free(MATRIX_PIN_Y3);
	gpio_free(MATRIX_PIN_Y4);

	printk(KERN_INFO "matrix: module exited\n");
}

module_init(matrix_init);
module_exit(matrix_exit);

MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("Matrix Keypad Demo");
MODULE_LICENSE("GPL");
