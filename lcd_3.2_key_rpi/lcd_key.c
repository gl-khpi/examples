#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/pinctrl/pinctrl-state.h>

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("LCD Key Demo");

#define LCD_PIN_COUNT 3

// default pins
//#define LCD_KEY1_PIN 18
//#define LCD_KEY2_PIN 23
//#define LCD_KEY3_PIN 24


struct lcd_key_dev {
	struct platform_device* dev;
	unsigned int pins[LCD_PIN_COUNT];
	int irqs[LCD_PIN_COUNT];
};

static struct lcd_key_dev lcd_key_dev;

static int lcd_key_probe(struct platform_device* pdev)
{
	int ret;
	struct device *dev = &pdev->dev;
	struct device_node *node = dev->of_node;

    	lcd_key_dev.dev = pdev;

	ret = of_property_read_u32_array(node, "pins", lcd_key_dev.pins, LCD_PIN_COUNT);
	if (!ret){
		printk(KERN_INFO "LCD_KEY: Used pins: %d %d %d\n", lcd_key_dev.pins[0], lcd_key_dev.pins[1], lcd_key_dev.pins[2]);
	} else {
		printk(KERN_WARNING "LCD_KEY: Used default pins(18, 23, 24): %d\n", ret);
		lcd_key_dev.pins[0] = 18;
		lcd_key_dev.pins[1] = 23;
		lcd_key_dev.pins[2] = 24;
	}

	dev_info(&pdev->dev, "device probed\n");

	return 0;
}

static int lcd_key_remove(struct platform_device* pdev)
{

	printk(KERN_INFO "LCD_KEY: driver removed");
	return 0;
}

static const struct of_device_id lcd_key_of_match[] = {
    { .compatible = "lcd-key", },
    {},
};

MODULE_DEVICE_TABLE(of, lcd_key_of_match);

static struct platform_driver lcd_key_driver = {
    .probe = lcd_key_probe,
    .remove = lcd_key_remove,
    .driver = {
        .name = "lcd-key-drv",
        .of_match_table = of_match_ptr(lcd_key_of_match),
        .owner = THIS_MODULE,
    },
};

static irqreturn_t lcd_key_isr(int irq, void *data)
{
	u8 y = 0;

	y = gpio_get_value(lcd_key_dev.pins[0]);
	printk(KERN_INFO "LCD_KEY: key1=%d\n", y);
	y = gpio_get_value(lcd_key_dev.pins[1]);
	printk(KERN_INFO "LCD_KEY: key2=%d\n", y);
	y = gpio_get_value(lcd_key_dev.pins[2]);
	printk(KERN_INFO "LCD_KEY: key3=%d\n", y);

	return IRQ_HANDLED;
}

static int lcd_key_get_gpio(void) {
	int i, ret;

	for (i = 0; i < LCD_PIN_COUNT; i++) {

		ret = gpio_request(lcd_key_dev.pins[i], "LCD_KEY_PIN");
		if (ret) {
			printk(KERN_ERR "LCD_KEY: failed to request GPIO%d: %d\n", lcd_key_dev.pins[i], ret);
			return -1;
		}

		gpio_direction_input(lcd_key_dev.pins[i]);

		ret = gpio_to_irq(lcd_key_dev.pins[i]);
		if (ret < 0) {
			printk(KERN_ERR "LCD_KEY: failed to request IRQ: %d\n", ret);
			return -1;
		}

		lcd_key_dev.irqs[i] = ret;
		printk(KERN_INFO "LCD_KEY: requested IRQ#%d.\n", lcd_key_dev.irqs[i]);
		ret = request_irq(lcd_key_dev.irqs[i], lcd_key_isr, IRQF_TRIGGER_FALLING, "lcd_key_irq", NULL);
		if (ret) {
			printk(KERN_ERR "LCD_KEY: failed to request IRQ\n");
			return -1;
		}
	}

	return 0;
}

static int lcd_key_free_gpio(void) {
	int i;

	for (i = 0; i < LCD_PIN_COUNT; i++) {
		free_irq(lcd_key_dev.irqs[i], NULL);

		gpio_free(lcd_key_dev.pins[i]);
	}

	return 0;
}

static int __init lcd_key_init(void)
{

	if (platform_driver_register(&lcd_key_driver)) {
		printk(KERN_ERR "LCD_KEY: failed to register driver");
		return EINVAL;
	}

	if (lcd_key_get_gpio()) {
		return EINVAL;
	}

	printk(KERN_INFO "LCD_KEY: module loaded\n");
	return 0;
}

static void __exit lcd_key_exit(void)
{

	platform_driver_unregister(&lcd_key_driver);

	lcd_key_free_gpio();

	printk(KERN_INFO "LCD_KEY: module exited\n");
}

module_init(lcd_key_init);
module_exit(lcd_key_exit);
  