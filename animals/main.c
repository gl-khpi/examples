/**
 * @file main.c
 * Опис структури тварини. Реалізація функції генерації
 * масиву структур та виводу його на екран.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

/**
 * Кількість записів в перерахуванні кількості тварин.
 */
const int ANIMAL_TYPE_COUNT = 4;

/**
 * Тип тварини
 */
enum animal_type {
	PIG, ///< Свиня
	COW, ///< Корова
	DOG, ///< Собака
	CAT ///< Кіт
};

/**
 * Структура Тварина
 */
struct animal {
	enum animal_type type; ///< тип тварини
	uint32_t height; ///< ріст тварини, см
	uint32_t weight; ///< маса тварини, грам
};

/**
 * Отримання текстового представлення значення типу тварини
 * @param type значення перерахувааня типу тварини
 * @return текстове репрезентування типу
 */
char *get_animal_type_name(enum animal_type type)
{
	char *result;
	switch (type) {
	case CAT:
		result = "Кіт";
		break;
	case DOG:
		result = "Собака";
		break;
	case COW:
		result = "Корова";
		break;
	case PIG:
		result = "Свиня";
		break;
	default:
		result = "N/A";
	}
	return result;
}

/**
 * Генерує тварину з випадковими значеннями.
 * @return структура {@link animal} з заповненими полями
 */
struct animal generate_animal()
{
	struct animal result;
	result.height = random() % INT8_MAX;
	result.weight = random() % INT8_MAX;
	result.type = random() % ANIMAL_TYPE_COUNT;
	return result;
}

/**
 * Виводить дані на екран про кожний елемент масиву
 * з даними про тварину у наступному форматі:
 * "Тип_тварини: зріст = ріст_тварини см, маса = маса_тварини гр."
 * @param animals масив з даними о тваринах, які необхідно вивести на екран
 * @param count кількість тварин у переданому масиві
 */
void show_animals(struct animal animals[], int count)
{
	for (int i = 0; i < count; i++) {
		printf("Інформація про тварину №%02d: ", i + 1);
		printf("%s: зріст = %d см, маса = %d гр. \n",
		       get_animal_type_name(animals[i].type), animals[i].height,
		       animals[i].weight);
	}
}

/**
 * Послідовність дій:
 * - створення масиву із 10 тварин
 * - генерація даних для кожної тварини шляхом виклика функції {@link generate_animal}
 * - вивід даних про всіх тварин на екран за допомогою функції {@link show_animals}
 * @return успішний код повернення з програми (0)
 */
int main()
{
	srandom(time(0));
	const int ANIMAL_COUNT = 10; // кількість тварин
	struct animal animals[ANIMAL_COUNT];

	for (int i = 0; i < ANIMAL_COUNT; i++) {
		animals[i] = generate_animal();
	}
	show_animals(animals, ANIMAL_COUNT);

	return 0;
}
