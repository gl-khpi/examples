#ifndef __XPT2046_H__
#define __XPT2046_H__

#define LCD_PIN_CS 7
#define LCD_PIN_RESET 27
#define LCD_PIN_DC 22

#define LCD_PIN_INT 17
#define LCD_PIN_KEY1 18
#define LCD_PIN_KEY2 23
#define LCD_PIN_KEY3 24

// screen orientation
#define XPT2046_SCALE_X 320
#define XPT2046_SCALE_Y 240

// calibration
#define XPT2046_MIN_RAW_X 3400
#define XPT2046_MAX_RAW_X 29000
#define XPT2046_MIN_RAW_Y 3300
#define XPT2046_MAX_RAW_Y 30000

#define XPT2046_READ_X 0x90
#define XPT2046_READ_Y 0xD0


#endif // __XPT2046_H__